#include <iostream>
#include <ctime>

int main()
{
    using namespace std;

    cout << "enter the delay time in seconds: ";
    float secs;
    cin >> secs;
    clock_t delay = secs * CLOCKS_PER_SEC;  //convert to clock ticks

    cout << "starting\a" << endl;

    clock_t start = clock();
    while(clock() - start < delay)
        ;   //just wait
    
    cout << "done" << endl;

    return 0;
}