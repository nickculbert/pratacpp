#include <iostream>
#include <cmath>
#include "coordin.h"

//convert rectangular to polar
polar rect_to_polar(rect xypos)
{
    using namespace std;
    polar answer;

    answer.distance = sqrt(xypos.x * xypos.x + xypos.y * xypos.y);
    answer.angle = atan2(xypos.y, xypos.x);
    return answer;
}

//show polar ciirdunates, converting angle to degrees from radians
void show_polar(polar dapos)
{
    using namespace std;
    const double Rad_to_deg = 57.29577951;

    cout << "Distance: " << dapos.distance << endl;
    cout << "Angle: " << dapos.angle * Rad_to_deg << "degrees" <<endl; 
}