#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

//struct definition
struct CandyBar
{
    string brand;
    float weight;
    int calories;
};

int main()
{
    CandyBar myBars[3] = {"Mocha Munch", 2.3, 250,
                            "KitKat", 4.1, 340,
                            "Oh Henry", 3.8, 410};

    cout << "Candybar #1: " << endl;
    cout << "Brand: " << myBars[0].brand << endl;
    cout << "Weight: " << myBars[0].weight << endl;
    cout << "Calories: " << myBars[0].calories << endl;
    cout << endl;
    cout << "Candybar #2: " << endl;
    cout << "Brand: " << myBars[1].brand << endl;
    cout << "Weight: " << myBars[1].weight << endl;
    cout << "Calories: " << myBars[1].calories << endl;
    cout << endl;
    cout << "Candybar #3: " << endl;
    cout << "Brand: " << myBars[2].brand << endl;
    cout << "Weight: " << myBars[2].weight << endl;
    cout << "Calories: " << myBars[2].calories << endl;
    cout << endl;

    

    return 0;
}