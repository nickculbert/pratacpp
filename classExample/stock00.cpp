#include <iostream>
#include <string>
#include "stock00.h"


//standard CONSTRUCTOR
Stock::Stock(const std::string & co, long n, double pr)
{
    company = co;

    if(n < 0)
    {
        std::cerr << "Number of stocks can't be less than zero; " << company 
        << " shares set to zero" << std::endl; 

        shares = 0;        
    }
    else
    {
        shares = n;
    }

    share_val = pr;
    set_tot();    
}

//Default constructor - required due to having a standard constructor
Stock::Stock()
{
    company = "No name";
    shares = 0;
    share_val = 0;
    total_val = 0;
}

//Destructor - Would add deletes and such if required
Stock::~Stock()
{}

void Stock::acquire(const std::string & co, long n = 0, double pr = 0)
{
    company = co;
    if(n < 0)
    {
        std::cout << "Number of shares can't be negative; "
                    << company << " shares set to 0." << std::endl;
        shares = 0;
    }

    else
    {
        shares = n;
    }
    
    share_val = pr;

    set_tot();
}

void Stock::buy(long num, double price)
{
    if(num < 0)
    {
        std::cout << "Number of shares can't be negative; "
                << "Transaction is aborted." << std::endl;
    }
    else
    {
        shares = shares + num;
        share_val = price;
        set_tot();
    }
}

void Stock::sell(long num, double price)
{
    if(num < 0)
    {
        std::cout << "Number of shares can't be negative; "
                << "Transaction is aborted." << std::endl;
    }
    else if(num > shares)
    {
        std::cout << "Can't sell more than you have; "
                << "Transaction is aborted." << std::endl;
    }
    else
    {
        shares = shares - num;
        share_val = price;
        set_tot();
    }
}

void Stock::update(double price)
{
    share_val = price;
    set_tot();
}

void Stock::show()
{
    using std::cout;
    using std::ios_base;
    // set format to #.###
    ios_base::fmtflags orig = cout.setf(ios_base::fixed, ios_base::floatfield);
    std::streamsize prec = cout.precision(3);


    cout << "Company: " << company << std::endl;
    cout << "Shares: " << shares << std::endl;
    cout << "Share price: " << share_val << "$" <<  std::endl;
    //set to #.###
    cout.precision(2);
    cout << "Total value: " << total_val << "$" <<  std::endl;
    cout << std::endl;

    //restore original format
    cout.setf(orig, ios_base::floatfield);
    cout.precision(prec);
}