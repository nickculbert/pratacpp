#include <iostream>

using std::cin;
using std::cout;
using std::endl;

//prototpe
float CtoF(float);

//main
int main()
{
    float C = 0;
    cout << "Enter temperature in celcius: ";
    cin >> C;
    cout << C << " degrees C is " << CtoF(C) << " degrees F" << endl;

    return 0;
}

//definitions
float CtoF(float C)
{
    return (1.8 * C) + 32;
}