#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

struct Pizza
{
    string brand;
    int diameter;
    double weight;
};

int main()
{
    //create struct array with new

    Pizza *myPie = new Pizza [3];

    cout << "Enter info for pizza 1:" << endl;
    cout << "Enter pizza diameter: ";
    cin >> myPie[0].diameter;
    cin.get();  //discard new line
    cout << "Enter pizza brand: "; 
    getline(cin, myPie[0].brand);
    cout << "Enter pizza weight: ";
    cin >> myPie[0].weight;
    cin.get();

    cout << "Enter info for pizza 2:" << endl;
    cout << "Enter pizza diameter: ";
    cin >> myPie[1].diameter;
    cin.get();  //discard new line
    cout << "Enter pizza brand: "; 
    getline(cin, myPie[1].brand);
    cout << "Enter pizza weight: ";
    cin >> myPie[1].weight;
    cin.get();

    cout << "Enter info for pizza 3:" << endl;
    cout << "Enter pizza diameter: ";
    cin >> myPie[2].diameter;
    cin.get();  //discard new line
    cout << "Enter pizza brand: "; 
    getline(cin, myPie[2].brand);
    cout << "Enter pizza weight: ";
    cin >> myPie[2].weight;
    cin.get();

    cout << "Pizza information: " << endl;
    cout << "Pizza 1: " << endl;
    cout << "Brand: " << myPie[0].brand <<endl;
    cout << "Diameter: " << myPie[0].diameter << endl;
    cout << "Weight: " << myPie[0].weight << endl;
    cout << "Pizza 2: " << endl;
    cout << "Brand: " << myPie[1].brand <<endl;
    cout << "Diameter: " << myPie[1].diameter << endl;
    cout << "Weight: " << myPie[1].weight << endl;
    cout << "Pizza 3: " << endl;
    cout << "Brand: " << myPie[2].brand <<endl;
    cout << "Diameter: " << myPie[2].diameter << endl;
    cout << "Weight: " << myPie[2].weight << endl;

    delete [] myPie;

    return 0;
}