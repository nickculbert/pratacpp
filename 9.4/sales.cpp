#include <iostream>
#include "sales.h"

void SALES::setSales(Sales & s, const double ar[], int n)
{
    s.min = 0; 
    s.max = 0;
    s.average = 0;
    //store array data
    for(int i = 0; i < 4; i++)
    {
        if(i<n)
            s.sales[i] = ar[i];
        else
        {
           s.sales[i] = 0;
        }
    }

    //determine min and max
    s.min = s.sales[0];
    for(int i = 0; i < n; i++)
    {
        if(s.sales[i] < s.min)
            s.min = s.sales[i];
        if(s.sales[i] > s.max)
            s.max = s.sales[i];
    }

    //calculate avg
    for(int i = 0; i < n; i++)
    {
        s.average = s.average + s.sales[i];
    }
    s.average = s.average/n;
}

void SALES::setSales(Sales & s)
{
    //collect data from user
    s.min = 0; 
    s.max = 0;
    s.average = 0;
    int n = 0;

    for(int i = 0; i < 4; i++)
    {
        std::cout << "Enter sales #" << i + 1 << ": ";
        std::cin >> s.sales[i];
        if(std::cin)
            n++;
    }

    //determine min and max
    s.min = s.sales[0];
    for(int i = 0; i < n; i++)
    {
        if(s.sales[i] < s.min)
            s.min = s.sales[i];
        if(s.sales[i] > s.max)
            s.max = s.sales[i];
    }

    //calculate avg
    for(int i = 0; i < n; i++)
    {
        s.average = s.average + s.sales[i];
    }
    s.average = s.average/n;
}

void SALES::showSales(const Sales & s)
{
    using std::cout;
    using std::endl;

    cout << "Sales: ";
    for(int i = 0; i < 4; i++)
    {
        cout << s.sales[i];
        if(i < 3)
            cout << ", ";
    }
    cout << endl;
    cout << "Min: " << s.min << endl;
    cout << "Max: " << s.max << endl;
    cout << "Average: " << s.average << endl;
}