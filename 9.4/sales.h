#ifndef SALES_H_
#define SALES_H_


const int QUARTERS = 4;

namespace SALES
{
    struct Sales
    {
        double sales[QUARTERS];
        double average;
        double max;
        double min;
    };

    //function copies the lesser of 4 or n items from the array ar to the sales member of s
    //and computes and stores the average, maximum, and minimum values of the entered items.
    //remaining elements are set to 0, if there are any
    void setSales(Sales & s, const double ar[], int n);

    //function gathers sales for 4 quarters interactively, stores them in the sales member of s,
    //and computes and stores the average, min, and max values
    void setSales(Sales & s);

    //display all information in structure s
    void showSales(const Sales & s);
}

#endif