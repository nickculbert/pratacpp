#include <iostream>
#include "sales.h"

using std::cin;
using std::cout;
using std::endl;

int main()
{
    SALES::Sales sales1;
    SALES::Sales sales2;

    double sa[QUARTERS] = {1.3, 2.6 , 3.2, 4.9};

    SALES::setSales(sales1, sa, 3);     //only do the first 3 elements of the array
    SALES::showSales(sales1);

    SALES::setSales(sales2);
    SALES::showSales(sales2);
    
    return 0;
}