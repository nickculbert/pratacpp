#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

const string MONTHS[12] = {"January", "February", "March", "April", "May", 
                            "June", "July", "August", "September", "October",
                            "November", "December"};

int main()
{
    int sales[3][12] = {0};
    int total = 0;

    for(int j = 0; j < 3; j++)
    {
        for(int i = 0; i < 12; i++)
        {
            cout << "Input sales for the month of " << MONTHS[i] << ", year " << j+1 <<": ";
            cin >> sales[j][i];
        }
    }

    for(int j = 0; j < 3; j++)
    {
        for(int i = 0; i < 12; i++)
        {
            total = total + sales[j][i];
        }
    }

    cout << "Total sales: " << total << " units" << endl;

    return 0;
}