#include <iostream>
#include "plorg.h"

using std::cin;
using std::cout;
using std::endl;

int main()
{
    cout << "Default Plorg" << endl;
    Plorg myp;
    myp.show();

    cout << "Custom Plorg" << endl;
    char customname[MAX_NAME_LEN];
    cout << "Enter custom name: ";
    cin.getline(customname, MAX_NAME_LEN);
    Plorg custom(customname);
    custom.show();
    cout << "Enter a new CI: ";
    int newc = 0;
    cin >> newc;
    custom.changeCI(newc);
    custom.show();
    

    return 0;
}