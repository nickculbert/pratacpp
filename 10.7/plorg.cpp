#include <iostream>
#include <cstring>
#include "plorg.h"

using std::cout;
using std::endl;

Plorg::Plorg(const char * n, int c)
{
    strcpy(name, n);
    ci = c;
}

void Plorg::changeCI(int newCi)
{
    ci = newCi;
}

void Plorg::show() const
{
    cout << "Plorg data: " << endl;
    cout << "Name: " << name << endl;
    cout << "CI: " << ci << endl;
}