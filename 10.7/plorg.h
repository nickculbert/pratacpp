
static const int MAX_NAME_LEN = 19;

class Plorg
{
    private:
        char name[MAX_NAME_LEN];
        int ci;

    public:
        //constructor
        Plorg(const char * n = "Plorga", int c = 50);
        void changeCI(int newCi);
        void show() const;
};