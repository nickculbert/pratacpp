#include <iostream>
#include "move.h"

Move::Move(double a, double b)
{
    x = a;
    y = b;  
}

void Move::showMove() const
{
    using std::cout;
    using std::endl;

    cout << "X: " << x << endl;
    cout << "Y: " << y << endl;
}

Move Move::add(const Move & m)
{
    Move store(this->x + m.x, this->y + m.y);
    return store;
}

void Move::reset(double a, double b)
{
    x = a;
    y = b;
}