

class Move
{
    private:
        double x;
        double y;

    public:
        Move(double a = 0, double b = 0);      //sets x,y to a,b
        void showMove() const;                  //show current x,y values
        Move add(const Move & m);               //this function adds x of m to x of the involking object
                                                //and the same for y, then returns the new values as a 
                                                //new Move object
        void reset(double a = 0, double b = 0); //reset x,y to a,b
};