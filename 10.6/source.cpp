#include <iostream>
#include "move.h"

int main()
{
    Move move1(1,1);
    move1.showMove();
    
    Move move2(3,3);
    move2.showMove();

    std::cout << "Adding them together: " << std::endl;
    Move move3(move1.add(move2));
    move3.showMove();

    return 0;
}