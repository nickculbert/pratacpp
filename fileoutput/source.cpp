//-----------------------------------------------------------------
//
//  Example on how to output to a file
//
//-----------------------------------------------------------------

#include <iostream>
#include <fstream>

int main()
{
    using namespace std;

    char automobile[50];
    int year;
    double a_price;
    double d_price;

    ofstream outFile;               //create output
    outFile.open("carinfo.txt");    //associate to file

    cout << "Enter the make and model of automobile: ";
    cin.getline(automobile, 50);
    cout << "Enter model year: ";
    cin >> year;
    cout << "Enter original price: ";
    cin >> a_price;
    d_price = 0.913 * a_price;

    //display on screen
    cout << fixed;  //fixed precision
    cout.precision(2);  //2 decimal points
    cout.setf(ios_base::showpoint);
    cout << "Make and model: " << automobile << endl;
    cout << "Year: " << year << endl;
    cout << "Original price: " << a_price << endl;
    cout << "New price: " << d_price << endl;

    //output to file
    outFile.fixed;
    outFile.precision(2);  //2 decimal points
    outFile.setf(ios_base::showpoint);
    outFile << "Make and model: " << automobile << endl;
    outFile << "Year: " << year << endl;
    outFile << "Original price: " << a_price << endl;
    outFile << "New price: " << d_price << endl;

    outFile.close();

    return 0;
}