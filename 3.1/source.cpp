#include <iostream>

using std::cin;
using std::cout;
using std::endl;

const int INCHES_IN_FOOT = 12;

int main()
{
    int heightAllInches = 0;

    cout << "Enter a height in all inches:___\b\b\b";
    cin >> heightAllInches;
    cout << heightAllInches << " = " << heightAllInches / INCHES_IN_FOOT << " ft, " 
    << heightAllInches % INCHES_IN_FOOT <<" inches" << endl;

    return 0;
}