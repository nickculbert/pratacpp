#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main()
{
    string firstname;
    string lastname;
    string fullname;

    cout << "Enter first name: ";
    getline(cin, firstname);
    cout << "Enter last name: ";
    getline(cin, lastname);
    fullname = lastname + ", " + firstname;

    cout << "Full name: " << fullname << endl;

    return 0;
}