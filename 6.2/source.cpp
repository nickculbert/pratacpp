#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    float donations[10] = {0.};
    int validDonations = 0;

    for(int i = 0; i < 10; i++)
    {
        cout << "Enter donation #" << i+1 << ": ";
        cin >> donations[i];
        if(cin)
        {
            validDonations++;
        }
        else
        {
            cin.clear();
            break;
        }   
    }

    //calculate average
    float avg = 0.;
    for(int i = 0; i < validDonations; i++)
    {
        avg = avg + donations[i];
    }
    avg = avg / validDonations;
    cout << "Average donation: " << avg << endl;

    //calculate number of fonations larger than average
    int largerThanAvg = 0;
    for(int i = 0; i < validDonations; i++)
    {
        if(donations[i] > avg)
            largerThanAvg++;
    }
    cout << "Number of donations larger than average: " << largerThanAvg << endl;
    

    return 0;
}