#include <iostream>
#include "golf.h"

int main()
{
    using namespace std;
    char n[] = "Fake Name";
    int defhc = 5;

    cout << "Default constructor:" << endl;
    Golf P1;
    P1.showGolf();

    cout << "Custom with no hc given:" << endl;
    Golf P2(n);
    P2.showGolf();

    cout << "Custom with an HC given: " << endl;
    Golf P3(n, defhc);
    P3.showGolf();

    cout << "Change HC" << endl;
    P3.sethandicap(2);
    P3.showGolf();


    return 0;
}