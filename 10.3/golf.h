#ifndef GOLF_H_
#define GOLF_H_

const int LEN = 40;

class Golf
{
    private:
        char fullname[LEN];
        int handicap;
    public:
        Golf() {fullname[0] = '\0', handicap = 0;}
        Golf(const char * name, int hc = 0);
        ~Golf();
        void sethandicap(int hc);
        void showGolf() const;
};

#endif