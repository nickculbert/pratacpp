#include <cstring>
#include <iostream>
#include "golf.h"

Golf::Golf(const char * name, int hc)
{
    strcpy(fullname, name);
    handicap = hc;
}

Golf::~Golf()
{
    
}

void Golf::sethandicap(int hc)
{
    handicap = hc;
}

void Golf::showGolf() const
{
    using namespace std;
    cout << "Name: " << fullname << endl;
    cout << "Handicap: " << handicap << endl;
}