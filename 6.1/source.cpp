#include <iostream>
#include <cctype>
#include <cstring>

using std::cin;
using std::cout;
using std::endl;

const int MAX_STR_LEN = 40;

int main()
{
    char myStr[MAX_STR_LEN] = {' '};

    cout << "Enter string of characters: ";
    cin.getline(myStr, MAX_STR_LEN);

    //perform printing function with cctype
    int len = strlen(myStr);

    for(int i = 0; i < len; i++)
    {
        if(myStr[i] == '@')
            break;
        if(islower(myStr[i]))
            cout << (char)toupper(myStr[i]);
        if(isupper(myStr[i]))
            cout << (char)tolower(myStr[i]);
        if(isspace(myStr[i]))
            cout << " ";
    }

    return 0;
}