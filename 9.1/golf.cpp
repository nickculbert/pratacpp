#include <iostream>
#include <cstring>
#include "golf.h"

void setgolf(golf & g, const char * name, int hc)
{
    strcpy(g.fullname, name);
    g.handicap = hc;
}

int setgolf(golf & g)
{
    using namespace std;

    cout << "Enter name: ";
    cin.getline(g.fullname, LEN);
    cout << "Enter handicap: ";
    cin >> g.handicap;
    cin.get();
    if(g.fullname[0 == '\0'])
        return 0;
    else
    {
        return 1;
    }  
}

void handicap(golf & g, int hc)
{
    g.handicap = hc;
}

void showgolf(const golf & g)
{
    using namespace std;

    cout << "Name: " << g.fullname << endl;
    cout << "Handicap: " << g.handicap << endl;
}