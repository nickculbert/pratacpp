#include <iostream>
#include "golf.h"

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int hc = 0;
    char name[LEN] = {"default"};

    golf myplayer;
    cout << "Enter name: ";
    cin.getline(name, LEN);
    cout << "Enter handicap: ";
    cin >> hc;
    cin.get();
    cout << "Non interactive function: " << endl;
    setgolf(myplayer, name, hc);
    showgolf(myplayer);

    cout << "Interactive version: " << endl;
    cout << setgolf(myplayer) << endl;
    showgolf(myplayer);

    cout << "Change handicap: " << endl;
    handicap(myplayer, 5);
    showgolf(myplayer);

    return 0;
}