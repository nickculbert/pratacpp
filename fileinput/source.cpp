//---------------------------------------------------------------
//
//  Simple file input example
//
//---------------------------------------------------------------

#include <iostream>
#include <fstream>
#include <cstdlib>

const int SIZE = 60;

int main()
{
    using namespace std;

    char filename[SIZE];
    ifstream inFile;
    cout << "Enter name of data file: ";
    cin.getline(filename, SIZE);
    inFile.open(filename);

    //check if opened
    if(!inFile.is_open())   //if failed
    {
        cout << "Could not open file" << endl;
        cout << "Terminating..." << endl;
        return(EXIT_FAILURE);
    }

    double value;
    double sum = 0.0;
    int count = 0;  //items read

    inFile >> value;    //get first value
    while(inFile.good())    //while input is good
    {
        cout << value << endl;
        ++count;
        sum = sum + value;
        inFile >> value;    //read next value
    }

    //if EOF
    if(inFile.eof())
    {
        cout << "End of file reached" << endl;
    }

    //if input failed for other reason
    else if(inFile.fail())
    {
        cout << "Input terminated for invalid input" << endl;
    }
    else
    {
        cout << "Input terminated for unknown reason" << endl;
    }

    //check counts
    if(count == 0)
    {
        cout << "No data processed" << endl;
    }
    else
    {
        cout << "Items read: " << count << endl;
        cout << "Sum: " << sum << endl;
        cout << "Average: " << sum / count << endl;
    }

    inFile.close();
    
    return 0;   
}