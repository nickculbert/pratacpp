#include <iostream>

using std::cin;
using std::cout;
using std::endl;

const int MAX_SCORE_NUM = 10;

//prototypes
int fillscores(int scores[], int n);
void displayscores(const int scores[], int n);
double avgscores(const int scores[], int n);

//main
int main()
{
    int scores[MAX_SCORE_NUM] = {0};
    int validScores = 0;

    validScores = fillscores(scores, MAX_SCORE_NUM);
    displayscores(scores, validScores);
    cout << "Average: " << avgscores(scores, validScores);

    return 0;
}

//definitions
int fillscores(int scores[], int n)
{
    int i = 0;
    //int validcnt = 0;
    cout << "Enter scores to be stored: " << endl;
    while(cin && i < n)
    {
        cout << "Enter score #" << i+1 << ": ";
        cin >> scores[i];
        if(cin)
        {
            i++;
        }
    }
    cin.clear();
    return i;   //number of valid scores
}

void displayscores(const int scores[], int n)
{
    cout << "Scores: ";
    for(int i = 0; i < n; i++)
    {
        cout << scores[i];
        if(i < n-1)
            cout << ", ";
    }
    cout << endl << endl;
}

double avgscores(const int scores[], int n)
{
    double avg = 0.;
    for(int i = 0; i < n; i++)
    {
        avg = avg + scores[i];
    }
    avg = avg / n;
    return avg;
}