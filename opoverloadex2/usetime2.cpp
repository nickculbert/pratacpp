#include <iostream>
#include "mytime2.h"

int main()
{
    using std::cout;
    using std::endl;
    Time weeding(4, 35);
    Time waxing(2, 47);
    Time total;
    Time diff;
    Time adjusted;

    cout << "Weeding time = ";
    weeding.show();
    cout << endl;
    
    cout << "Waxing time = ";
    waxing.show();
    cout << endl;

    cout << "Total work time = ";
    total = weeding + waxing;   //using operator+()
    total.show();
    cout << endl;

    diff = weeding - waxing;       //using operator-()
    cout << "Weeding time - waxing time = ";
    diff.show();
    cout << endl;

    adjusted = total * 1.5;     //use operator*()
    cout << "Adjusted work time = ";
    adjusted.show();
    cout << endl;

    return 0;
}