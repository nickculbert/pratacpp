#ifndef VECT_H_
#define VECT_H_

#include <iostream>

namespace VECTOR
{
    class Vector
    {
        public:
            enum Mode {RECT, POL};  //RECT for rectangular, POL for polar

        private:
            double x;       //horizontal value
            double y;       //vertical value
            double mag;     //length of vector
            double ang;     //angle of vector in degrees
            Mode mode;      //RECT or POL
            //private methods for setting values
            void set_mag();
            void set_ang();
            void set_x();
            void set_y();

        //methods
        public:
            Vector();
            Vector(double n1, double n2, Mode form = RECT);
            void reset(double n1, double n2, Mode form = RECT);
            ~Vector();
            double xval() const {return x;}    // returns x value
            double yval() const {return y;}
            double magval() const {return mag;}
            double angval() const {return ang;}
            void polar_mode();      //set mode to POL
            void rect_mode();       //set mode to RECT

            //overload operators
            Vector operator+(const Vector & b) const;
            Vector operator-(const Vector & b) const;
            Vector operator-() const;
            Vector operator*(double n) const;

            //friends
            friend Vector operator*(double n, const Vector & a);    //allows multiplication by contants
            friend std::ostream & operator<<(std::ostream & os, const Vector & v);  //allows output
    };
}//end namespace VECTOR

#endif