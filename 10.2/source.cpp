#include <iostream>
#include <string>
#include "person.h"

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main()
{
    Person one;     //using default constructor
    Person two("Smythcraft");
    Person three("DimWiddy", "Sam");

    one.show();
    //cout << endl;
    one.formalShow();
    
    two.show();
    //cout << endl;
    two.formalShow();

    three.show();
    //cout << endl;
    three.formalShow();

    return 0;
}