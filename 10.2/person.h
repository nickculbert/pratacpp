#include <string>
using std::string;

class Person
{
    private:
        static const int LIMIT = 25;
        string lname;
        char fname[LIMIT];

    public:
        Person() {lname = ""; fname[0] = '\0';}   //default const
        Person(const string & ln, const char * fn = "HEYYOU");
        void show() const;
        void formalShow() const;
};