#include <string>
#include <iostream>
#include <cstring>
#include "person.h"

Person::Person(const string & ln, const char * fn)
{
    lname = ln;
    strcpy(fname, fn);
}

void Person::show() const
{
    using namespace std;
    cout << "Name: " << fname << " " << lname << endl;
}

void Person::formalShow() const
{
    using namespace std;
    cout << "Name: " << lname << ", " << fname << endl;
}