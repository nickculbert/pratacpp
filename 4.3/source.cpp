#include <iostream>
#include <cstring>

const int MAX_STR_LEN = 20;

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main()
{
    char firstname[MAX_STR_LEN];
    char lastname[MAX_STR_LEN];
    //char comma[2] = {','};
    char fullname[2*MAX_STR_LEN + 1];
    cout << "Enter first name: ";
    cin.getline(firstname, MAX_STR_LEN);
    cout << "Enter last name: ";
    cin.getline(lastname, MAX_STR_LEN);

    strcat(fullname, lastname);
    strcat(fullname, ", ");
    strcat(fullname, firstname);

    cout << "Fullname: " << fullname << endl;

    return 0;
}