#include <iostream>

using std::cin;
using std::cout;
using std::endl;

const int MAX_STR_LEN = 40;

struct box
{
    char maker[MAX_STR_LEN];
    float height;
    float width;
    float length;
    float volume;
};

//Prototypes
void displaybox(const box);
void setboxvolume(box *);


//main
int main()
{
    box myBox = {"Boxco", 2,3,4,0};

    displaybox(myBox);
    setboxvolume(&myBox);
    displaybox(myBox);

    return 0;
}

//defs
void displaybox(const box myBox)
{
    cout << "Box data: ";
    cout << "Maker: " << myBox.maker << endl;
    cout << "Height: " << myBox.height << endl;
    cout << "Width: " << myBox.width << endl;
    cout << "Length: " << myBox.length << endl;
    cout << "Volume: " << myBox.volume << endl;
}

void setboxvolume(box *myBox)
{
    cout << "Setting box volume" << endl;
    myBox->volume = myBox->length * myBox->height * myBox->width;
}
