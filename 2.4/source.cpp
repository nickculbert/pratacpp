#include <iostream>

const int MONTHS_IN_YEAR = 12;

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int ageYears = 0;
    cout << "Enter your age: ";
    cin >> ageYears;
    cout << "Age in months: " << ageYears * MONTHS_IN_YEAR << endl;

    return 0;
}