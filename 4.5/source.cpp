#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

//struct definition
struct CandyBar
{
    string brand;
    float weight;
    int calories;
};

int main()
{
    CandyBar myBar = {"Mocha Munch", 2.3, 250};

    cout << "Brand: " << myBar.brand << endl;
    cout << "Weight: " << myBar.weight << endl;
    cout << "Calories: " << myBar.calories << endl;

    return 0;
}