#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

struct Car
{
    string make;
    int year;
};

int main()
{
    int count = 0;

    cout << "How many cars would you like to catalogue: ";
    cin >> count;
    cin.get();

    Car *catalogue = new Car [count];

    cout << "Input car data: " << endl;
    for(int i = 0; i < count; i++)
    {
        cout << "Car #" << i+1 << ":" << endl;
        cout << "Input make: ";
        getline(cin, catalogue[i].make);
        cout << "Input year: ";
        cin >> catalogue[i].year;
        cin.get();
        cout << endl;
    }

    cout << "Car catalogue: " << endl;
    for(int i = 0; i < count; i++)
    {
        cout << "Car #" << i+1 << ": " << catalogue[i].year << ", " << catalogue[i].make << endl;
    }

    delete [] catalogue;

    return 0;
}