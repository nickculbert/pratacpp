#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

struct Pizza
{
    string brand;
    int diameter;
    double weight;
};

int main()
{
    Pizza myPie;
    cout << "Enter pizza brand: ";
    getline(cin, myPie.brand);
    cout << "Enter diameter: ";
    cin >> myPie.diameter;
    cout << "Enter weight: ";
    cin >> myPie.weight;

    cout << "Pizza information: " << endl;
    cout << "Brand: " << myPie.brand << endl;
    cout << "Diameter: " << myPie.diameter << endl;
    cout << "Weight: " << myPie.weight << endl;

    return 0;
}