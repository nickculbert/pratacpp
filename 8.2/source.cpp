#include <iostream>
#include <cstring>

using std::cin;
using std::cout;
using std::endl;

const int MAX_STR_LEN = 40;

struct candybar
{
    char brand[MAX_STR_LEN];
    float weight;
    int calories;
};

//protos
void setbar(candybar &, char *, float, int);
void showbar(const candybar &);


//main
int main()
{
    candybar mybar = {"default", 0, 0};
    char name[MAX_STR_LEN];
    int cal = 0;
    float weight = 0.;
    showbar(mybar);

    cout << "Set candybar data: " << endl;
    cout << "Input brand: ";
    cin.getline(name, MAX_STR_LEN);
    cout << "Enter weight: ";
    cin >> weight;
    cout << "Enter calories: ";
    cin >> cal;
    cin.get();

    //set bar
    setbar(mybar, name, weight, cal);
    showbar(mybar);


    return 0;
}

//defs
void setbar(candybar & mybar, char * name, float w, int c)
{
    strcpy(mybar.brand, name);
    mybar.weight = w;
    mybar.calories = c;
}

void showbar(const candybar & mybar)
{
    cout << "Candy bar data: " << endl;
    cout << "Brand: " << mybar.brand << endl;
    cout << "Weight: " << mybar.weight << endl;
    cout << "Calories: " << mybar.calories << endl;
}