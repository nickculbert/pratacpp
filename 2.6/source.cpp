#include <iostream>

using std::cin;
using std::cout;
using std::endl;

float LYtoAU(float);

int main()
{
    float LY = 0;
    cout << "Enter a distance in light years: ";
    cin >> LY;
    cout << LY << " light years = " << LYtoAU(LY) << " astronomical units" << endl;

    return 0;
}

float LYtoAU(float a)
{
    return a * 63240;
}