#include <iostream>
#include <string>
#include "account.h"

using std::string;
using std::endl;
using std::cout;
using std::cin;

int main()
{
    cout << "Exmaple of default account: " << endl;
    Account myAcc1;
    myAcc1.show();

    cout << "Example of user set account:" << endl;
    string myName;
    int myAcc = 0;
    double myBal = 0.0;
    cout << "Enter a name: ";
    getline(cin, myName);
    cout << "Enter account number: ";
    cin >> myAcc;
    cout << "Enter an opening balance: ";
    cin >> myBal;
    
    Account myAcc2(myName, myAcc, myBal);
    myAcc2.show();

    cout << "Show withdrawal and deposit: ";
    cout << "Deposit 300$" << endl;
    myAcc2.deposit(300);
    myAcc2.show();
    cout << "Withdraw 200$ " << endl;
    myAcc2.withdraw(200);
    myAcc2.show();


    return 0;
}