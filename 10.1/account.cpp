#include <iostream>
#include <string>
#include "account.h"

Account::Account(const std::string & n, int acc, double bal)
{
    name = n;
    accountNum = acc;
    balance = bal;
}

Account::~Account()
{
    std::cout << "Invoking destructor..." << std::endl;
}

void Account::deposit(double dep)
{
    if(dep < 0)
    {
        std::cout << "Deposit can't be negative; transaction aborted" << std::endl;
    }
    else
    {
        balance = balance + dep;
    }
}

void Account::withdraw(double take)
{
    if(take < 0)
    {
        std::cout << "Withdrawal can't be negative; transaction aborted" << std::endl;
    }
    else
    {
        balance = balance - take;
    }
}

void Account::show() const
{
    // set format to #.##
    using std::ios_base;
    ios_base::fmtflags orig = std::cout.setf(ios_base::fixed, ios_base::floatfield);
    std::streamsize prec = std::cout.precision(2);

    std::cout << "Account owner: " << name << std::endl;
    std::cout << "Account number: " << accountNum << std::endl;
    std::cout << "Balance: " << balance << std::endl;

    //restore original format
    std::cout.setf(orig, ios_base::floatfield);
    std::cout.precision(prec);
}