#include <string>

class Account
{
    private:
        std::string name;
        int accountNum;
        double balance;

    public:
        Account(const std::string & n = "Default", int acc = 0, double bal = 0.0); //constructor
        ~Account(); //destructor
        void deposit(double dep);
        void withdraw(double take);
        void show() const;
};