#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

struct Pizza
{
    string brand;
    int diameter;
    double weight;
};

int main()
{
    //create struct with new

    Pizza *myPie = new Pizza;

    cout << "Enter pizza diameter: ";
    cin >> myPie->diameter;
    cin.get();  //discard new line
    cout << "Enter pizza brand: "; 
    getline(cin, myPie->brand);
    cout << "Enter pizza weight: ";
    cin >> myPie->weight;
    cin.get();

    cout << "Pizza information: " << endl;
    cout << "Brand: " << myPie->brand <<endl;
    cout << "Diameter: " << myPie->diameter << endl;
    cout << "Weight: " << myPie->weight << endl;

    delete myPie;

    return 0;
}