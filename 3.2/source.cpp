#include <iostream>

const int INCHES_IN_FOOT = 12;
const float METERS_IN_INCH = 0.0254;
const float LBS_IN_KG = 2.2;

using std::cin;
using std::cout;
using std::endl;

int main()
{
    float heightFt = 0;
    float heightIn = 0;
    float heightAllIn = 0;
    float heightMeters = 0;
    float kg = 0;
    float lbs = 0;
    float BMI = 0;

    cout << "Enter height: " << endl;
    cout << "Ft: ";
    cin >> heightFt;
    cout << "In: ";
    cin >> heightIn;
    heightAllIn = (heightFt * INCHES_IN_FOOT) + heightIn;
    cout << "Enter weight in lbs: ";
    cin >> lbs;
    kg = lbs / LBS_IN_KG;
    heightMeters = heightAllIn * METERS_IN_INCH;
    BMI = kg / (heightMeters * heightMeters);

    cout << "BMI: " << BMI << endl;

    return 0;
}