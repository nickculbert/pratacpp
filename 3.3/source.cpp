#include <iostream>

const int SEC_IN_MIN = 60;
const int MIN_IN_DEG = 60;

using std::cin;
using std::cout;
using std::endl;

int main()
{
    float AllDegrees = 0;
    int degrees = 0;
    int minutes = 0;
    int seconds = 0;

    cout << "Enter latitude in degrees, minutes, seconds:" << endl;
    cout << "Degrees: ";
    cin >> degrees;
    cout << "Minutes: ";
    cin >> minutes;
    cout << "Seconds: ";
    cin >> seconds;

    AllDegrees = degrees + ((float)minutes / MIN_IN_DEG) + ((float)seconds / (MIN_IN_DEG * SEC_IN_MIN));

    cout << degrees << " degrees, " << minutes << " minutes, " << seconds << " seconds = " 
    << AllDegrees << " degrees" << endl;

    return 0;
}