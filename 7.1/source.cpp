#include <iostream>

using std::cin;
using std::cout;
using std::endl;

double harmonicMean(double a, double b);

int main()
{
    double a = 0., b = 0.;
    do
    {
        cout << "Enter first number: ";
        cin >> a;
        cout << "Enter second number: ";
        cin >> b;
        if(a == 0 || b == 0)
        {
            break;
        }
        else
        {
            cout << "Harmonic mean: " << harmonicMean(a,b) << endl;
        }
        
    } while (a != 0 && b != 0);
    

    return 0;
}

double harmonicMean(double a, double b)
{
    return (2 * a * b) / (a + b);   //return harmonic mean
}