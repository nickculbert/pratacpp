#include <iostream>

const int YARDS_IN_FURLONG = 220;

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int furlongs = 0;

    cout << "Input a distance in furlongs: ";
    cin >> furlongs;
    cout << furlongs << " furlongs = " << furlongs * YARDS_IN_FURLONG << " yards" << endl;

    return 0;
}