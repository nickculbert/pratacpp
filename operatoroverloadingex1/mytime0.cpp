#include <iostream>
#include "mytime0.h"

Time::Time()
{
    hours = minutes = 0;
}

Time::Time(int h, int m)
{
    hours = h;
    minutes = m;
}

void Time::addMin(int m)
{
    minutes = minutes + m;
    hours = hours + (minutes / 60);
    minutes = minutes % 60;
}

void Time::addHr(int h)
{
    hours = hours + h;
}

void Time::reset(int h, int m)
{
    hours = h;
    minutes = m;
}

Time Time::sum(const Time & t) const
{
    Time sum;
    sum.minutes = minutes + t.minutes;
    sum.hours = hours + t.hours + (sum.minutes/60);
    sum.minutes = minutes % 60;
    return sum;
}

void Time::show() const
{
    std::cout << hours << " hours, " << minutes << " minutes";
}