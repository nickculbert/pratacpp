#include <iostream>
#include <cctype>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

//protos
void stringswap(string &);

//mains
int main()
{
    string myStr = " ";
    while(myStr != "q" && myStr != "Q")
    {
        cout << "Enter a string: ";
        getline(cin, myStr);
        if(myStr == "q" || myStr == "Q")
            break;
        
        else
        {
            stringswap(myStr);
            cout << myStr << endl;
        } 
    }
}

//defs
void stringswap(string & myString)
{
    for(int i = 0; i < myString.length(); i++)
    {
        if(islower(myString[i]))
        {
            myString[i] = toupper(myString[i]);
        }
        else if (isupper(myString[i]))
        {
            myString[i] = tolower(myString[i]);
        }
    }
}