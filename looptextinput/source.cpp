#include <iostream>

int main()
{
    using namespace std;

    char ch;
    int count = 0;

    cout << "Enter characters, enter # to quit: ";
    cin.get(ch);    //fill input stream, count include spaces

    while(ch != '#')
    {
        cout << ch;
        ++count;
        cin.get(ch);    //get next character in input stream
    }

    cout << endl << count << " characters counted" << endl;

    cin.get(ch);
    cout << "Next remaining character: " <<  ch << endl;

    return 0;
}