#ifndef STACK_H_
#define STACK_H_

typedef unsigned long Item;

struct Customer
{
    char fullname[35];
    double payment;
};

class Stack
{
    private:
        enum {MAX = 10};    //constant specific to class
        Customer items[MAX];    //holds stack items
        int top;
    
    public:
        Stack();
        bool isempty() const;
        bool isfull() const;
        bool push(const Customer & cust);   //add item to stack, returns false if stack is already full
        bool pop(Customer & cust);  //pop top into item, returns false if stack already item
};

#endif