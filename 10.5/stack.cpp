#include "stack.h"

Stack::Stack()  //create empty stack
{
    top = 0;
}

bool Stack::isempty() const
{
    return top == 0;
}

bool Stack::isfull() const
{
    return top == MAX;
}

bool Stack::push(const Customer & cust)
{
    if(top < MAX)
    {
        items[top++] = cust;
        return true;
    }
    else
    {
        return false;
    }
}

bool Stack::pop(Customer & cust)
{
    if(top > 0)
    {
        cust = items[--top];
        return true;
    }
    else
    {
        return false;
    }
}