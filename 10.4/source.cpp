#include <iostream>
#include "sales.h"

using std::cin;
using std::cout;
using std::endl;

int main()
{
    SALES::Sales sales1;
    SALES::Sales sales2;

    double sa[SALES::QUARTERS] = {1.3, 2.6 , 3.2, 4.9};

    sales1.setSales(sa, 3);     //only do the first 3 elements of the array
    sales1.showSales();

    sales2.setSales();
    sales2.showSales();
    
    return 0;
}