namespace SALES
{
    const int QUARTERS = 4;
    class Sales
    {
        private:
            double sales[QUARTERS];
            double average;
            double max;
            double min;
        public:
            //function copies the lesser of 4 or n items from the array ar to the sales member of s
            //and computes and stores the average, maximum, and minimum values of the entered items.
            //remaining elements are set to 0, if there are any
            void setSales(const double ar[], int n);

            //function gathers sales for 4 quarters interactively, stores them in the sales member of s,
            //and computes and stores the average, min, and max values
            void setSales();

            //display all information in structure s
            void showSales();
    };
}
