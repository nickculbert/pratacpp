#include <iostream>
#include "sales.h"


void SALES::Sales::setSales(const double ar[], int n)
{
    min = 0; 
    max = 0;
    average = 0;
    //store array data
    for(int i = 0; i < 4; i++)
    {
        if(i<n)
            sales[i] = ar[i];
        else
        {
           sales[i] = 0;
        }
    }

    //determine min and max
    min = sales[0];
    for(int i = 0; i < n; i++)
    {
        if(sales[i] < min)
            min = sales[i];
        if(sales[i] > max)
            max = sales[i];
    }

    //calculate avg
    for(int i = 0; i < n; i++)
    {
        average = average + sales[i];
    }
    average = average/n;
}

void SALES::Sales::setSales()
{
    //collect data from user
    min = 0; 
    max = 0;
    average = 0;
    int n = 0;

    for(int i = 0; i < 4; i++)
    {
        std::cout << "Enter sales #" << i + 1 << ": ";
        std::cin >> sales[i];
        if(std::cin)
            n++;
    }

    //determine min and max
    min = sales[0];
    for(int i = 0; i < n; i++)
    {
        if(sales[i] < min)
            min = sales[i];
        if(sales[i] > max)
            max = sales[i];
    }

    //calculate avg
    for(int i = 0; i < n; i++)
    {
        average = average + sales[i];
    }
    average = average/n;
}

void SALES::Sales::showSales()
{
    using std::cout;
    using std::endl;

    cout << "Sales: ";
    for(int i = 0; i < 4; i++)
    {
        cout << sales[i];
        if(i < 3)
            cout << ", ";
    }
    cout << endl;
    cout << "Min: " << min << endl;
    cout << "Max: " << max << endl;
    cout << "Average: " << average << endl;
}
    