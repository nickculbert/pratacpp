#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    char selection = ' ';

    while(selection != 'q' && selection != 'Q')
    {
        cout << "Please enter one of the following: " << endl;
        cout << "c) carnivore       p) pianist" << endl;
        cout << "t) tree            g) game" << endl;
        cout << "q) quit" << endl;
        cout << "Selection: ";
        cin >> selection;
        
        switch(selection)
        {
            case 'c':
            case 'C':
                cout << "A shark is a carnivore" << endl;
                break;
            case 'p':
            case 'P':
                cout << "Chopin was a pianist" << endl;
                break;
            case 't':
            case 'T':
                cout << "A maple is a tree" << endl;
                break;
            case 'g':
            case 'G':
                cout << "Castlevania is a game" << endl;
                break;
            case 'q':
            case 'Q':
                cout << "Quitting..." << endl;
                break;
            default:
                cout << "Not a valid entry, try again" << endl;
                break;
        }

    }

    return 0;
}