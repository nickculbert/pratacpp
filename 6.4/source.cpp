#include <iostream>

const int MAX_STR_LEN = 40;

using std::cin;
using std::cout;
using std::endl;

struct bop
{
    char fullname[MAX_STR_LEN];
    char title[MAX_STR_LEN];
    char bopname[MAX_STR_LEN];
    int pref;
};

int main()
{
    bop members[3] = {"Cathy McCarthy", "Manager", "Bossgirl", 1,
                        "John Smith", "Programmer", "Hacks", 3,
                        "Randall Murray", "janitor", "Cleaner", 2};
    
    //display data
    cout << "Welcome to the BOP member registry" << endl;
    char selection = ' ';
    while(selection != 'Q' && selection != 'q')
    {
        cout << "a) display by name         b) display by title" << endl;
        cout << "c) display by bopname      d) display by preference" << endl;
        cout << "q) quit" << endl;

        cin >> selection;

        switch(selection)
        {
            case 'a':
            case 'A':
                cout << "displaying members by name: " << endl;
                for(int i = 0; i < 3; i++)
                {
                    cout << members[i].fullname << endl;
                }
                cout << endl;
                break;
            case 'b':
            case 'B':
                cout << "displaying members by title: " << endl;
                for(int i = 0; i < 3; i++)
                {
                    cout << members[i].title << endl;
                }
                cout << endl;
                break;
            case 'c':
            case 'C':
                cout << "displaying members by bopname: " << endl;
                for(int i = 0; i < 3; i++)
                {
                    cout << members[i].bopname << endl;
                }
                cout << endl;
                break;
            case 'd':
            case 'D':
                cout << "displaying members by preference: " << endl;
                for(int i = 0; i < 3; i++)
                {
                    if(members[i].pref == 1)
                        cout << members[i].fullname << endl;
                    if(members[i].pref == 2)
                        cout << members[i].title << endl;
                    if(members[i].pref == 3)
                        cout << members[i].bopname << endl;
                }
                cout << endl;
                break;
            case 'q':
            case 'Q':
                cout << "Exiting..." << endl;
                cout << endl;
                break;
            default:
                cout << "Not a valid entry..." << endl << endl;
                break;
        }
    }

    return 0;
}