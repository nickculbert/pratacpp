#ifndef STACK_H_
#define STACK_H_

typedef unsigned long Item;

class Stack
{
    private:
        enum {MAX = 10};    //constant specific to class
        Item items[MAX];    //holds stack items
        int top;
    
    public:
        Stack();
        bool isempty() const;
        bool isfull() const;
        bool push(const Item & item);   //add item to stack, returns false if stack is already full
        bool pop(Item & item);  //pop top into item, returns false if stack already item
};

#endif