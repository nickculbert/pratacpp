#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

const int MAX_STR_LEN = 20;

int main()
{
    char firstname[MAX_STR_LEN];
    string lastname;
    int age = 0;
    char grade = 'f';

    cout << "input first name: ";
    cin.getline(firstname, MAX_STR_LEN);
    cout << "Enter last name: ";
    getline(cin, lastname);
    cout << "Enter grade deserved: ";
    cin >> grade;
    cout << "enter age: ";
    cin >> age;
    
    cout << "Name: " << lastname << ", " << firstname << endl;
    cout << "Age: " << age << endl;
    cout << "Grade: " << (char)(grade+1);


    return 0;
}