#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int a = 0, b = 0;
    int sum = 0;

    cout << "This program calculates the sum of all numbers between a and b." << endl;
    cout << "Enter first integer: ";
    cin >> a;
    cout << "Enter second integer: ";
    cin >> b;

    for (int i = a; i <= b; i++)
    {
        sum = sum + i;
    }

    cout << "The sum of numbers between " << a << " and " << b << " is: " << sum << endl;
    
    

    return 0;
}