#include <iostream>
#include "stock00.h"

//default constructor
Stock::Stock()
{
    company = "default";
    shares = 0;
    share_val = 0.0;
    total_val = 0.0;
}
//constructor
Stock::Stock(const std::string & co, long n = 0, double pr = 0.0)
{
    company = co;
    if(n < 0)
    {
        std::cout << "Number of shares can't be negative; "
        << company << " shares set to 0 \n";
        shares = 0;
    }
    else
    {
        shares = n;
    }
    share_val = pr;
    set_tot();
}

//destructor
Stock::~Stock()
{
    //doesnt need to do anything, say bye for visual in example
    std::cout << "Bye..." << std::endl;
}

void Stock::acquire(const std::string & co, long n, double pr)
{
    company = co;
    if(n < 0)
    {
        std::cout << "Number of shares can't be negative; "
        << company << " shares set to 0 \n";
        shares = 0;
    }
    else
    {
        shares = n;
    }
    share_val = pr;
    set_tot();
}

void Stock::buy(long num, double price)
{
    if(num < 0)
    {
        std::cout << "Number of shares bought can't be negative; "
        << "Transaction aborted...\n";
    }
    else
    {
        shares = shares + num;
        share_val = price;
        set_tot();
    }
}

void Stock::sell(long num, double price)
{
    if(num < 0)
    {
        std::cout << "Number of shares sold can't be negative; "
        << "Transaction aborted...\n";
    }
    else if(num > shares)
    {
        std::cout << "Number of shares sold can't be more than owned; "
        << "Transaction aborted...\n";
    }
    else
    {
        shares = shares - num;
        share_val = price;
        set_tot();
    }
}

void Stock::update(double price)
{
    share_val = price;
    set_tot();
}

void Stock::show()
{
    using std::cout;
    using std::ios_base;
    //sett format to #.###
    ios_base::fmtflags orig = cout.setf(ios_base::fixed, ios_base::floatfield);
    std::streamsize prec = cout.precision(3);

    std::cout << "Company: " << company << "  Shares: " << shares << std::endl;
    std::cout << "Share price: $" << share_val;
    //set format to #.##
    cout.precision(2);
    cout << "  Total worth: $" << total_val << std::endl;

    //restore original formating
    cout.setf(orig, ios_base::floatfield);
    cout.precision(prec);
}