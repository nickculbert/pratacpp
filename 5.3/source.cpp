#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    double a = 0;
    double sum = 0;

    do
    {
        cout << "Enter a value to add to running sum, enter 0 to quit: ";
        cin >> a;
        sum = sum + a;
        cout << "Running sum: " << sum << endl; 
    } while (a != 0);
    

    return 0;
}